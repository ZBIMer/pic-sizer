﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicSizer.Static
{
    public static class PicStrings
    {
        public const string COMPRESS_ERROR = "压缩过程中发生错误.";

        public const string COMPRESS_CANNOT_RESIZE_TO_LIMIT_SIZE = "无法压缩到指定大小.";


    }
}
