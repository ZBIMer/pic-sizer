﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicSizer.Static
{
    public static partial class PicSetting
    {
        /// <summary>
        /// 图片背景色
        /// </summary>
        public static byte[] BackgroundColor = { 255, 255, 255 };
    }
}
